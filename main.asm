section .text

%include 'colon.inc'

section .data

%include 'words.inc'

message_start:
    db 'Введите ключ: ', 0
message_found: 
    db 'Значение по введенному ключу: ', 0
message_not_found: 
    db 'Ключ не найден', 10, 0
message_error: 
    db 'Ошибка при чтении слова', 10, 0

%define buffer_size 256
%define reserved_bytes 8

section .text

global _start

extern find_word
extern read_string
extern print_string
extern print_error_string
extern print_newline
extern string_length
extern exit

_start:
    mov rdi, message_start
    call print_string
    sub rsp, buffer_size
    mov rsi, buffer_size
    mov rdi, rsp
    call read_string
    test rax, rax
    jz .print_error
    mov rdi, rsp
    mov rsi, current
    call find_word
    add rsp, buffer_size
    test rax, rax
    jz .not_found
    push rax
    mov rdi, message_found
    call print_string
    pop rax
    add rax, reserved_bytes
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    call exit

.print_error:
    add rsp, buffer_size
    mov rdi, message_error
    jmp print_error_string_exit

.not_found:
    mov rdi, message_not_found
    jmp print_error_string_exit

.print_error_string_exit
    call print_error_string
    call exit
