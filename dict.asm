%define reserved_bytes 8

section .text

global find_word

extern string_length
extern string_equals

find_word:
.loop:
    push rdi
    push rsi
    add rsi, reserved_bytes
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jz .continue
    mov rax, rsi
    ret
.continue:
    mov rsi, [rsi]
    test rsi, rsi
    jnz .loop
    xor rax, rax
    ret
