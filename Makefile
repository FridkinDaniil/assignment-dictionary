flags = felf64
linker = ld
compiler = nasm

all: main.o lib.o dict.o
	$(linker) -o main $^

main.o: main.asm lib.o dict.o words.inc colon.inc
	$(compiler) -$(flags) -o main.o main.asm

dict.o: dict.asm lib.o
	$(compiler) -$(flags) -o  dict.o dict.asm

%.o: %.inc
	$(compiler) -$(flags) -o  $@ $<

clean:
	rm *.o
