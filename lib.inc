section .text

global exit
global string_length
global print_string
global print_error_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ; Код системного вызова "Выход"
    syscall     ; Системный вызов
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
.loop:
    cmp byte[rax+rdi], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push 0
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, rsp
    push 0
    sub rsp, 24
    mov r8, 10
    dec rdi
.loop:
    mov rdx, 0
    div r8
    add rdx, '0'
    dec rdi
    mov byte[rdi], dl
    cmp rax, 0
    jne .loop
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .plus
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.plus:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 0
    mov rdx, 0
.loop:
    mov al, byte[rdi]
    mov dl, byte[rsi]
    inc rdi
    inc rsi
    cmp al, dl
    je .check_end
    mov rax, 0
    ret
.check_end:
    cmp al, 0
    jne .loop
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 0
    mov rax, 0
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    mov r8, 0
.loop:
    push r8
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop r8
    cmp rax, 0x9
    je .continue
    cmp rax, 0xA
    je .continue
    cmp rax, 0x20
    je .continue
    cmp rax, 0
    je .end
    mov [rdi + r8], rax
    inc r8
    cmp r8, rsi
    jge .error
    jmp .loop
.continue:
    cmp r8, 0
    je .loop
.end:
    mov rax, 0
    mov [rdi + r8], rax
    mov rdx, r8
    mov rax, rdi
    ret
.error:
    mov rax, 0
    mov rdx, 0
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину строки в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к строке нуль-терминатор
read_string:
    mov r8, 0
.loop:
    push r8
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop r8
    cmp rax, 0x9
    je .check_space
    cmp rax, 0xA
    je .check_newline
    cmp rax, 0x20
    je .check_space
    cmp rax, 0
    je .end
.continue:    
    mov [rdi + r8], rax
    inc r8
    cmp r8, rsi
    jl .loop
    jmp .error
.check_space:
    cmp r8, 0
    jne .continue
    jmp .loop
.check_newline:
    cmp rcx, 0
    jne .end
    jmp .loop
.error:
    mov rax, 0
    mov rdx, 0
    ret
.end:
    mov rax, 0
    mov [rdi + r8], rax
    mov rax, rdi
    mov rdx, r8
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    mov r8, 0
    mov r9, 10
.loop:
    mov r10, 0
    mov r10b, byte[rdi + r8]
    cmp r10b, '0'
    jb .end
    cmp r10b, '9'
    ja .end
    cmp r10b, 0
    je .end
    inc r8
    sub r10b, '0'
    mul r9
    add rax, r10
    jmp .loop
.end:
    mov rdx, r8
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r10, 0
    mov r10b, byte[rdi]
    inc rdi
    cmp r10b, '-'
    jne .plus
    push 1
    call parse_uint
    neg rax
    jmp .end
.plus:
    push 0
    dec rdi
    call parse_uint
.end:
    pop r8
    add rdx, r8
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jl .continue
    mov rax, 0
    ret
.continue:
    mov rdx, 0
    mov r8, 0
.loop:
    mov dl, byte[rdi+r8]
    mov byte[rsi+r8], dl
    inc r8
    cmp dl, 0
    jne .loop
    ret
