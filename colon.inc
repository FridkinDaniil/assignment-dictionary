%define current 0

%macro colon 2
    %%next: dq current
    %define current %%next
    db %1, 0
    %2:
%endmacro